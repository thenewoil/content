_This template is used to submit a correction to a piece of content_

### Link to content
_ex_ `apertatube.net/video`, `blog.thenewoil.org/blog-post` or `https://gitlab.com/thenewoil/content/-/blob/main/Show%20Notes/20220817?ref_type=heads`

### Timestamp
04:16

### Incorrect information
"You said that Signal doesn't have usernames."

### Corrected information
"Now they do."

### Sources (if applicable)
[Link to Signal Blog Post](#)
